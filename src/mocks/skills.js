export default {
    ENV: {
        Docker: {
            value: 100,
            max:500, 
        },
        Linux: {
            value: 170,
            max: 350,
        },
        Bash: {
            value: 80,
            max: 350,
        },
    },
    Front: {
        VueJs: {
            value: 750,
            max: 1000, 
        },
        ReactJs: {
            value: 100,
            max: 1000,
        },
        CSS: {
            value: 350,
            max: 400,
        },
    },
    PHP: {
        yii2: {
            value: 350,
            max: 500, 
        },
        Symphony: {
            value: 100,
            max: 500,
        },
        Legacy: {
            value: 700,
            max: 800,
        },
    },
    DB: {
        MySql: {
            value: 115,
            max: 700, 
        },
        MariaDB: {
            value: 350,
            max: 500,
        },
        PostgreSql: {
            value: 50,
            max: 500,
        },
    }
}