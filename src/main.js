import Vue from 'vue'
import App from './App.vue'
import store from './store'

import '@/assets/styles/main.scss';

new Vue({
  render: h => h(App),
  el: '#app',
  store
})
