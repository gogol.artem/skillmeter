import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    totalRange: 0,
    maxRange: 0
  },
  getters: {
    GET_TOTAL_RANGE: state => {
      return state.totalRange;
    },
    GET_MAX_RANGE: state => {
      return state.maxRange;
    },
  },
  mutations: {
    SET_TOTAL_RANGE: (state, payload) => {
      state.totalRange += payload;
    },
    SET_MAX_RANGE: (state, payload) => {
      state.maxRange += payload;
    },
  },
  actions: {
    SET_TOTAL_RANGE: (context, payload) => {
      context.commit('SET_TOTAL_RANGE', payload);
    },
    SET_MAX_RANGE: (context, payload) => {
      context.commit('SET_MAX_RANGE', payload);
    },
  },
})
