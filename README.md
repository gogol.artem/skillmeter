# skillmeter

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### To create a docker container image
```
docker build . -t skillmeter
```
### To run docker container
```
docker run -p 3000:8080 skillmeter
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
